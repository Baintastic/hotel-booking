var app = angular.module("myApp", []);
app.controller("myCtrl", function ($scope, $http) {
    //Get All Bookings
    $scope.getAllBookings = function () {
        $http({
            method: "get",
            url: "http://localhost:1234/booking"
        }).then(function (response) {
            $scope.result = response.data;
        }, function () {
            //alert("Error Occur");
        });
    };

    //Get Booking
    $scope.getBooking = function () {
        $http({
            method: "get",
            url: "http://localhost:1234/booking/" + $scope.searchFilter
        }).then(function (response) {
            $scope.result = response.data;
        }, function () {
        });
    };

    //Delete Booking By Booking ID
    $scope.deleteBooking = function (id) {
        var request = $http({
            method: "delete",
            url: "http://localhost:1234/booking/" + id
        }).then(function (response) {
            $scope.getAllBookings();
        });
    };

    //Add New Booking
    $scope.addBooking = function () {
        $scope.Booking = {};
        $scope.Booking.RoomType = $scope.roomType;
        $scope.Booking.MealType = $scope.mealType;
        $scope.Booking.CheckInTime = $scope.checkInTime;
        $scope.Booking.CheckOutTime = $scope.checkOutTime;
        $scope.Booking.Price = $scope.price;
        $http({
            method: "post",
            url: "http://localhost:1234/booking",
            data: JSON.stringify($scope.Booking)
        }).then(function (response) {
            $scope.getAllBookings();
            $scope.roomType = "";
            $scope.mealType = "";
            $scope.checkInTime = "";
            $scope.checkOutTime = "";
            $scope.price = "";
        })
    };

    $scope.OpenUpdateScreen = function (id) {
        $scope.ShowHide(true);
        $scope.id = id;
    }

    $scope.updateBooking = function (id) {
        $scope.Booking = {};
        $scope.Booking.BookingId = id
        $scope.Booking.RoomType = $scope.roomType;
        $scope.Booking.MealType = $scope.mealType;
        $scope.Booking.CheckInTime = $scope.checkInTime;
        $scope.Booking.CheckOutTime = $scope.checkOutTime;
        $scope.Booking.Price = $scope.price;
        $http({
            method: "put",
            url: "http://localhost:1234/booking/" + id,
            data: JSON.stringify($scope.Booking)
        }).then(function (response) {
            $scope.getAllBookings();
            $scope.id = "";
            $scope.roomType = "";
            $scope.mealType = "";
            $scope.checkInTime = "";
            $scope.checkOutTime = "";
            $scope.price = "";
        })
    }

    $scope.ShowHide = function (visible) {
        $scope.IsVisible = visible;
    }
});

