﻿using System;

namespace HotelBooking.Models
{
    public class BookingModel
    {
        public int BookingId { get; set; }
        public int RoomType { get; set; }
        public int MealType { get; set; }
        public DateTime CheckInTime { get; set; }
        public DateTime CheckOutTime { get; set; }
        public decimal Price { get; set; }
    }
}
