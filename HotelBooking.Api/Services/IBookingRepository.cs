﻿using HotelBooking.Models;
using System.Collections.Generic;

namespace HotelBooking.Services
{
    public interface IBookingRepository
    {
        IEnumerable<BookingModel> GetAllBookings();
        IEnumerable<BookingModel> GetBookingBySearch(string text);
        void AddBooking(BookingModel booking);
        void UpdateBooking(BookingModel booking);
        void DeleteBooking(int id);
    }
}
