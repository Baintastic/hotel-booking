﻿using Dapper;
using HotelBooking.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace HotelBooking.Services
{
    public class BookingRepository : IBookingRepository
    {
        private readonly string connectionString;
        public BookingRepository()
        {
            connectionString = @"Data Source=LAPTOP-9JJ4EO3D\SQLEXPRESS;Initial Catalog=HotelBooking;Integrated Security=True;";
        }

        public IEnumerable<BookingModel> GetAllBookings()
        {
            using (var connection = CreateDbConnection(connectionString))
            {
                var querySQL = "SELECT * FROM dbo.Booking";
                return connection.Query<BookingModel>(querySQL);
            }
        }

        public IEnumerable<BookingModel> GetBookingBySearch(string text)
        {
            using (var connection = CreateDbConnection(connectionString))
            {
                var querySQL = "SELECT * FROM dbo.Booking WHERE BookingId LIKE @SearchText " +
                               "OR RoomType LIKE @SearchText OR MealType LIKE @SearchText " +
                               "OR CheckInTime LIKE @SearchText OR Price LIKE @SearchText";
                return connection.Query<BookingModel>(querySQL, new { SearchText = text });
            }
        }

        public void AddBooking(BookingModel booking)
        {
            using (var connection = CreateDbConnection(connectionString))
            {
                connection.Open();
                var querySQL = "INSERT INTO dbo.Booking (RoomType, MealType, CheckInTime, CheckOutTime, Price) " +
                               "VALUES(@RoomType, @MealType, @CheckInTime, @CheckOutTime, @Price)";
                connection.Execute(querySQL, booking);
            }
        }

        public void UpdateBooking(BookingModel booking)
        {
            using (var connection = CreateDbConnection(connectionString))
            {
                connection.Open();
                var querySQL = "UPDATE dbo.Booking " +
                               "SET RoomType = @RoomType, MealType = @MealType, CheckInTime = @CheckInTime, CheckOutTime= @CheckOutTime, Price = @Price " +
                               "WHERE BookingId = "+ booking.BookingId;
                connection.Execute(querySQL, booking);
            }
        }

        public void DeleteBooking(int id)
        {
            using (var connection = CreateDbConnection(connectionString))
            {
                connection.Open();
                var querySQL = "DELETE FROM dbo.Booking WHERE BookingId = @BookingId";
                connection.Execute(querySQL , new { BookingId = id });
            }          
        }

        private static IDbConnection CreateDbConnection(string connectionString)
        {
            var connection = new SqlConnection(connectionString);
            return connection;
        }
    }
    
}
