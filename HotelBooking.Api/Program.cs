﻿using Nancy.Hosting.Self;
using System;

namespace HotelBooking
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var host = new NancyHost(new Uri("http://localhost:1234")))
            {
                host.Start();
                Console.WriteLine("NancyFX Stand alone test application.");
                Console.WriteLine("Press enter to exit the application");
                Console.WriteLine(DateTime.Now);
                Console.ReadLine();
            }
        }
    }
}
