﻿using HotelBooking.Services;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;

namespace HotelBooking
{
    public class CustomBootstrapper : DefaultNancyBootstrapper
    {
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            container.Register(typeof(IBookingRepository), typeof(BookingRepository));
            pipelines.AfterRequest += (ctx) =>
            {
                ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                    .WithHeader("Access-Control-Allow-Methods", "POST,GET,PUT,DELETE")
                    .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type");
            };
        }
    }

}
