﻿using HotelBooking.Models;
using HotelBooking.Services;
using Nancy;
using Nancy.Extensions;
using Nancy.ModelBinding;

namespace HotelBooking.Modules
{
    public class BookingModule : NancyModule
    {
        private readonly IBookingRepository _bookingRepository;

        public BookingModule(IBookingRepository bookingRepository)
            : base("/booking")
        {
            _bookingRepository = bookingRepository;

            Get("/", parameters =>
            {
                return _bookingRepository.GetAllBookings();
            });

            Get("/{text}", parameters =>
            {
                return _bookingRepository.GetBookingBySearch(parameters.text);
            });

            Post("/", parameters =>
            {
                var model = this.Bind<BookingModel>();
                _bookingRepository.AddBooking(model);
                var jsonString = this.Request.Body.AsString();
                return Response.AsJson(jsonString);
            });

            Put("/{id}", parameters =>
            {
                var model = this.Bind<BookingModel>();
                _bookingRepository.UpdateBooking(model);
                var jsonString = this.Request.Body.AsString();
                return Response.AsJson(jsonString);
            });

            Delete("/{id}", parameters =>
            {
                _bookingRepository.DeleteBooking(parameters.id);
                return HttpStatusCode.OK; //come back
            });
        }
    }
}

