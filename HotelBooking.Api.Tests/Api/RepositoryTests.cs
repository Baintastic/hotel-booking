﻿//using NUnit.Framework;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using HotelBooking.Modules;
//using HotelBooking.Services;
//using HotelBooking.Models;

//namespace HotelBooking.Api.Tests.Api
//{
//    [TestFixture]
//    public class RepositoryTests
//    {

//        [Test]
//        public void GetAllBookings_ShouldReturnAllFromFakeBookingRepository()
//        {
//            // Arrrange
//            var repository = new FakeBookingRepository();

//            // Act
//            var result = repository.GetAllBookings();

//            // Assert
//            Assert.AreEqual(2, result.Count());
//        }

//        [Test]
//        public void GetBookingById_ShouldReturnBookingRecordUsingIdFromFakeBookingRepository()
//        {
//            // Arrrange
//            var repository = new FakeBookingRepository();

//            // Act
//            var result = repository.GetBookingById(1);

//            // Assert
//            Assert.AreEqual(2, result.MealType);
//        }

//        [Test]
//        public void MockGetBookingById_ShouldReturnBookingRecordUsingIdFromFakeBookingRepository()
//        {
//            // Arrrange
//            var mockRepository = new Mock();

//            // Act
//            var result = repository.GetBookingById(1);

//            // Assert
//            Assert.AreEqual(2, result.MealType);
//        }

//        [Test]
//        public void AddBooking_ShouldReturnBookingRecordUsingIdFromFakeBookingRepository()
//        {
//            // Arrrange
//            var bookingModule = new FakeBookingRepository();
//            var bookingModel = new BookingModel
//            {
//                BookingId = 2,
//                MealType = 2,
//                RoomType = 2,
//                CheckInTime = DateTime.Now,
//                CheckOutTime = DateTime.Now.AddDays(1),
//                Price = 800
//            };

//            // Act
//           // bookingModule.AddBooking(bookingModel);
//            var result = bookingModule.GetBookingById(bookingModel.BookingId);

//            // Assert
//            Assert.NotNull(result);
//        }

//        [Test]
//        public void UpdateBooking_ShouldReturnBookingRecordUsingIdFromFakeBookingRepository()
//        {
//            // Arrrange
//            var bookingModule = new FakeBookingRepository();
//            var bookingModel = new BookingModel
//            {
//                BookingId = 2,
//                MealType = 2,
//                RoomType = 2,
//                CheckInTime = DateTime.Now,
//                CheckOutTime = DateTime.Now.AddDays(1),
//                Price = 800
//            };

//            // Act
//           // bookingModule.UpdateBooking(bookingModel);
//            var result = bookingModule.GetBookingById(bookingModel.BookingId);

//            // Assert
//            Assert.NotNull(result);
//        }

//    }
//}
