﻿using HotelBooking.Models;
using HotelBooking.Services;
using System;
using System.Collections.Generic;

namespace HotelBooking.Api.Tests.Api
{
    public class FakeBookingRepository : IBookingRepository
    {
        public void AddBooking(BookingModel booking)
        {
            throw new NotImplementedException();
        }

        public void DeleteBooking(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BookingModel> GetAllBookings()
        {
            return GetListOfBookings();
        }

        public BookingModel GetBookingById(int id)
        {
            return new BookingModel()
            {
                BookingId = id,
                MealType = 2,
                RoomType = 3,
                CheckInTime = DateTime.Now,
                CheckOutTime = DateTime.Now,
                Price = 200
            };
        }

        public IEnumerable<BookingModel> GetBookingBySearch(string text)
        {
            throw new NotImplementedException();
        }

        public void UpdateBooking(BookingModel booking)
        {
            throw new NotImplementedException();
        }

        private IEnumerable<BookingModel> GetListOfBookings()
        {
            return new List<BookingModel>(new[]
            {
                new BookingModel()
                {
                    BookingId = 1,
                    MealType =1,
                    RoomType =2,
                    CheckInTime = DateTime.Now,
                    CheckOutTime = DateTime.Now,
                    Price = 100
                },
                new BookingModel()
                {
                    BookingId = 2,
                    MealType =2,
                    RoomType =3,
                    CheckInTime = DateTime.Now,
                    CheckOutTime = DateTime.Now,
                    Price = 200
                }
            });
        }
    }
}