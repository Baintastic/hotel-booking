﻿using HotelBooking.Models;
using HotelBooking.Modules;
using HotelBooking.Services;
using Nancy;
using Nancy.Json;
using Nancy.Testing;
using NUnit.Framework;
using System;

namespace HotelBooking.Api.Tests.Api
{
    [TestFixture]
    public class ApiTests
    {
        IBookingRepository bookingRepository;
        [Test]
        public void Get_WhenRequested_ShouldReturnOKStatusCode()
        {
            //Arrange
            var fakeBookingRepository = new FakeBookingRepository();
            var browser = CreateBrowser(fakeBookingRepository);

            //Act
            var result = browser.Get("/booking", with => {
                with.HttpRequest();
                with.Header("Accept", "application/json");
            });

            //Assert
            Assert.AreEqual(HttpStatusCode.OK, result.Result.StatusCode);
        }

        [Test]
        public void Post_WhenSent_ShouldReturnOKStatusCode()
        {
            //Arrange
            bookingRepository = new BookingRepository();
            
            var fakeBookingRepository = new FakeBookingRepository();
            var module = new BookingModule(bookingRepository);
            var bookingModel = new BookingModel
            {
                BookingId = 2,
                MealType = 2,
                RoomType = 2,
                CheckInTime = DateTime.Now,
                CheckOutTime = DateTime.Now.AddDays(1),
                Price = 800
            };

            var json = new JavaScriptSerializer().Serialize(bookingModel);
            var browser = CreateBrowser(bookingRepository);

            //Act
            var result = browser.Post("/booking/", with => {
                with.Body(json);
                with.Header("content-type", "application/json");            
            });

            //Assert
            Assert.AreEqual(HttpStatusCode.OK, result.Result.StatusCode);
        }

        [Test]
        public void Put_WhenSent_ShouldReturnOKStatusCode()
        {
            //Arrange
            bookingRepository = new BookingRepository();

            var fakeBookingRepository = new FakeBookingRepository();
            var module = new BookingModule(bookingRepository);
            var bookingModel = new BookingModel
            {
                MealType = 2,
                RoomType = 3,
                Price = 600
            };

            var json = new JavaScriptSerializer().Serialize(bookingModel);
            var browser = CreateBrowser(bookingRepository);

            //Act
            var result = browser.Put("/2/", with => {
                with.Body(json);
                with.Header("content-type", "application/json");
            });

            //Assert
            Assert.AreEqual(HttpStatusCode.OK, result.Result.StatusCode);
        }

        [Test]
        public void Delete_WhenSent_ShouldReturnOKStatusCode()
        {
            //Arrange
            bookingRepository = new BookingRepository();
            var fakeBookingRepository = new FakeBookingRepository();

            var browser = CreateBrowser(bookingRepository);

            //Act
            var result = browser.Delete("/booking/2", with => {
                with.Header("content-type", "application/json");
            });

            //Assert
            Assert.AreEqual(HttpStatusCode.OK, result.Result.StatusCode);
        }

        public static Browser CreateBrowser(IBookingRepository fakeBookingRepository)
        {          
            var bootstrapper = new ConfigurableBootstrapper(with =>
            {
                with.Module<BookingModule>();
                with.Dependency(fakeBookingRepository);
            });
            var browser = new Browser(bootstrapper);
            return browser;
        }
    }
}
